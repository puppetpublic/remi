#
# Configuration specific to using packages from remi with a small custom EPEL
# repo.  This whole class is Red Hat specific.
# TODO: move to the new packages module area

class remi {
    include remi::remi-up2date

    base::rpm::import {
        remi-rpmkey:
            url       => "http://yum.stanford.edu/RPM-GPG-KEY-remi",
            signature => gpg-pubkey-00f97f56-467e318a,
    }
}


# override default up2date sources
class remi::remi-up2date inherits epel::epel-up2date {
    case $lsbdistrelease {
        4: {
            Base::Up2date::Sources["fqdn"] {
                jpackage17 => false,
                useRHN     => false,
                remi       => true,
                java5      => false,
                java6      => false,
                epel       => true,
            }
        }
    }
}